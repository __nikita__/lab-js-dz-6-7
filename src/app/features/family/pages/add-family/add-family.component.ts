import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../services/family.service';

@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {}

  public generateFamilyMemberFormGroup(memName = null, memAge = null) {
    return new FormGroup({
      name: new FormControl(memName, [Validators.required]),
      age: new FormControl(memAge, [Validators.required])
    });
  }

  public ngOnInit(): void {
    this.familyForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      father: this.generateFamilyMemberFormGroup(),
      mother: this.generateFamilyMemberFormGroup(),
      children: new FormArray([
        this.generateFamilyMemberFormGroup()
      ]),
    });
  }
  public addChild() {
    const lastChildIndex = this.familyForm.value.children.length-1;
    const child = this.familyForm.value.children[lastChildIndex];
    this.children.push(this.generateFamilyMemberFormGroup(child.name, child.age));
  }
  public removeChild(index: number) {
    this.children.removeAt(index);
  }
  public submit() {
    this.familyService.addFamily$(this.familyForm.value);
  }
}
